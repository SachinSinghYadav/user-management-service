## User management

User Management contains single modules to serve the functionality of Managing Users And there features.

## Environment Variables
- UM_CONFIG_SPRING_DATASOURCE_URI=localhost:3306/UserManagement
- UM_CONFIG_SPRING_DATASOURCE_USER=root
- UM_CONFIG_SPRING_DATASOURCE_PASSWORD=password


## Steps to setup locally
* clone the user-management-service repository to test the rest endpoints.(https://bitbucket.org/SachinSinghYadav/workspace/projects/US)

> git clone https://SachinSinghYadav@bitbucket.org/SachinSinghYadav/user-management-service.git

* Once you clone the repository, just follow below commands to make it work.

* cd user-management-service

* mvn clean install

After this it should be generated jar files in the target folder of the module.

* For starting up the user-management-service, Use the below command

> java -jar ./user-management-service/target/java -jar user-management-service-1.0.0-SNAPSHOT.jar


## Lombok framework to generate boilerplate codes

Project Lombok is used in this repository to generate boilerplate codes(for instance, Getters, Setters, EqualsAndHashCode, toString).
It is a java library tool which is used to minimize/remove the boilerplate code and save the precious time of developers during development by just using some annotations.In addition to it, it also increases the readability of the source code and saves space.
Whenever we use IDEs to generate these boilerplate codes, we just save ourselves from writing all these codes but it is actually present in our source code and increases the LOC (lines of code) and reduces maintainability and readability. On the other hand, Lombok adds all these boilerplate codes at the compile-time in the “.class” file and not in our source code.  
For more information on Lombok, refer the link: https://www.baeldung.com/intro-to-project-lombok 

##Set Up Lombok in IntelliJ

The Jetbrains IntelliJ IDEA editor is compatible with lombok.Follow below steps to set up Project Lombok in IntelliJ:

1. Add the Lombok IntelliJ plugin to add lombok support for IntelliJ: 
  •	Go to Preferences > Editor > Plugins 
  •	Click on Browse repositories... 
  •	Search for Lombok Plugin 
  •	Click on Install plugin 
  •	Restart IntelliJ IDEA 

2. Enabling Annotation Processing

Lombok uses annotation processing through APT, so, when the compiler calls it, the library generates new source files based on annotations in the originals.
Annotation processing isn't enabled by default, though.
So, the first thing for us to do is to enable annotation processing in our project.
We need to go to the Preferences | Build, Execution, Deployment | Compiler | Annotation Processors and make sure of the following:
•	Enable annotation processing box is checked
•	Obtain processors from project classpath option is selected

3. Add the Lombok maven dependency in the classpath
	<dependency>
		<groupId>org.projectlombok</groupId>
		<artifactId>lombok</artifactId>
		<version>1.18.12</version>
	</dependency>

4. Add the lombok.config file in the root directory. Use below annotations to improve the code coverage for the model classes:

a) config.stopBubbling = true
To stop lombok from looking at parent directories for more configuration files

b) lombok.addLombokGeneratedAnnotation = true

 Beginning with version 0.8.0, Jacoco can detect, identify, and ignore Lombok-generated code. The only thing you as the developer have to do is to create a file named lombok.config in your directory’s root
 This adds the annotation lombok.@Generated to the relevant methods, classes and fields. Jacoco is aware of this annotation and will ignore that annotated code.

c) lombok.equalsAndHashCode.doNotUseGetters = [true | false] (default: false)

If set to true, lombok will access fields directly instead of using getters (if available) when generating equals and hashCode methods. The annotation parameter 'doNotUseGetters', if explicitly specified, takes precedence over this setting.


## Basic Lombok Annotations

1. @Getter - To generate the getter for all the fields in the class.Place this annotation above the class to generate getters for all fields.
2. @Setter - To generate the setter for all the fields in the class.Place this annotation above the class to generate setters for all fields.
3. @EqualsAndHashCode - To generate the equals and hash code method for the class file.
4. @ToString - To generate the toString method for the class file.
5. @Getter(AccessLevel.NONE) - This is required in case we do not want lombok to generate Getter for that field and declare our own custom getter.
Place this annotation above the field name.
6. @Setter(AccessLevel.NONE) - This is required in case we do not want lombok to generate Setter for that field and declare our own custom setter.
Place this annotation above the field name.
7. @NoArgsConstructor - It will generate a constructor with no parameters.
8. @AllArgsConstructor - It generates a constructor with 1 parameter for each field in your class. Fields marked with @NonNull result in null checks on those parameters.
9. @Builder - This annotation produces complex builder APIs for your classes.
10. @Builder.Default - If a certain field/parameter is never set during a build session, then it always gets 0 / null / false. If you've put @Builder on a class (and not a method or constructor) you can instead specify the default directly on the field, and annotate the field with @Builder.Default
11. @Singular - The singular annotation is used together with @Builder to create single element 'add' methods in the builder for collections.              
