#Adding the Base Image
#FROM openjdk:8-jdk

FROM adoptopenjdk/maven-openjdk8


#Creating a Workign Directory
RUN mkdir -p /opt/apps/um/user-management-service

#Setting the Working Directory
WORKDIR /opt/apps/um/user-management-service

#Copying the project files.
COPY . .

#Building the project
RUN mvn clean install

#Changing the Working Directory
WORKDIR /opt/apps/um/user-management-service/target/

#Start up command to run executable .jar file
CMD ["java","-jar","user-management-service-1.0.0-SNAPSHOT.jar"]
