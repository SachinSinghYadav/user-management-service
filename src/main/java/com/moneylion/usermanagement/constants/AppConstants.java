package com.moneylion.usermanagement.constants;

/**
 * A class where common constants are declared for the entire service.
 * All the variable should have access modifier as public static final.
 * 
 * @author sachinsinghyadav
 * */
public class AppConstants {
	
	public static final String RESPONSE_MESSAGE_UPDATE = "Updated";
	public static final String RESPONSE_MESSAGE_CREATED = "Created";
}
