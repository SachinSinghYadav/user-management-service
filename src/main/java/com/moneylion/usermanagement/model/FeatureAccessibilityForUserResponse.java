package com.moneylion.usermanagement.model;

//+ 3rd Party Import(s)
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@ToString
public class FeatureAccessibilityForUserResponse {

	boolean canAccess;
}
