package com.moneylion.usermanagement.model;


//+ 3rd Party Import(s)
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CreateFeatureForUserRequest {
	
	@Size(max = 130, message = "FeatureName can not exceed 130 characters")
	@Pattern(regexp = "^[a-zA-Z ]*$", message = "FeatureName should be an alphabetic")
	private String featureName;
	
	@Size(max = 15, message = "Email Id should not exceed 50 alphamumerice characters")
	@Pattern(regexp = "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$", message = "Email should be alphamumerice characters")
	private String email;
	
	@Size(max = 5, message = "The probable values are true and false")
	@Pattern(regexp = "^[a-zA-Z]*$", message = "Enable Flag should be an alphabetic")
	private String enable;
}
