package com.moneylion.usermanagement.exceptions;

import org.springframework.http.HttpStatus;

public class UserManagementServiceException extends RuntimeException {

	private HttpStatus statusCode = HttpStatus.BAD_REQUEST;

	public UserManagementServiceException() {
	}

	public UserManagementServiceException(final String message) {
		super(message);
	}

	public UserManagementServiceException(HttpStatus statusCode, String message) {
		super(message);
		this.statusCode = statusCode;
	}

	public HttpStatus getStatusCode() {
		return statusCode;
	}
}
