package com.moneylion.usermanagement.resources;

//+ 3rd Party Import(s).
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

//+ Local Import(s)
import com.moneylion.usermanagement.model.CreateFeatureForUserRequest;
import com.moneylion.usermanagement.model.FeatureAccessibilityForUserResponse;
import com.moneylion.usermanagement.services.ManageUsersService;

/**
 * A controller class which holds two rest endpoints. The base url for the rest
 * endpoints is kept as http://{hostname}:{port}/{context-path}/rest/v1. Please
 * go through the README.md file to set these from enviroment variable or
 * application.yml
 * 
 * <p>
 * Types that carry this annotation are treated as controllers where
 * {@link RequestMapping @RequestMapping} methods assume
 * {@link ResponseBody @ResponseBody} semantics by default.
 * 
 * GET - /feature?featureName=XXX &email=XXX :- Used the fetch the accessibility
 * flag to the given feature for a particular user. POST - /feature :- Used to
 * add or updated the feature for a given user.
 * <p>
 * 
 * @see GetMapping
 * @see PostMapping
 *
 * @author sachinsinghyadav
 *
 */

@RestController
@RequestMapping(value = "/rest/v1")
public class ManageUsersResource {

	/**
	 * Initialising the logger instance.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(ManageUsersResource.class);

	private final ManageUsersService manageUsersService;

	/**
	 * Constructor Injection for the dependency.
	 * 
	 * @see https://docs.spring.io/spring-boot/docs/2.0.x/reference/html/using-boot-spring-beans-and-dependency-injection.html
	 */
	public ManageUsersResource(final ManageUsersService manageUsersService) {
		this.manageUsersService = manageUsersService;
	}

	/**
	 * The GET Mapping rest endpoint.
	 * <p>
	 * GET - /feature?featureName=XXX &email=XXX :- Used the fetch the accessibility
	 * flag to the given feature for a particular user.
	 * </p>
	 * 
	 * @return Object of type FeatureAccessibilityForUserResponse. example :- {
	 *         "canAccess" : true}
	 */
	@GetMapping(value = "/feature", produces = MediaType.APPLICATION_JSON_VALUE)
	public FeatureAccessibilityForUserResponse getFeatureAccessibilityForUser(
			@RequestParam(name = "email") String email, @RequestParam(name = "featureName") String featureName) {

		String logPrefix = "##getFeatureAccessibilityForUser(): ";
		LOGGER.info("{} : Get Feture Accessibility For Email [{}] And Feature Name : [{}]", logPrefix, email,
				featureName);
		return manageUsersService.fetchFeatureAccessibilityForUser(email, featureName);
	}

	/**
	 * The POST Mapping rest endpoint.
	 * <p>
	 * POST - /feature :- Used to add or updated the feature for a given user.
	 * </p>
	 * 
	 * @return Object of type ResponseEntity.
	 */
	@PostMapping(value = "/feature", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> saveNewFeatureForUser(
			@Valid @RequestBody CreateFeatureForUserRequest createFeatureForUserRequest) {
		String logPrefix = "##saveNewFeatureForUser(): ";
		LOGGER.info("{} : Create or Update New Feature As [{}]", logPrefix, createFeatureForUserRequest.toString());
		return manageUsersService.saveNewFeatureForUser(createFeatureForUserRequest);
	}

}
