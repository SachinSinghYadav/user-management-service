package com.moneylion.usermanagement.services;

//+ Standard Import(s).
import java.util.Optional;

//+ 3rd Party Import(s)
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

//+ Local Import(s).
import com.moneylion.usermanagement.model.CreateFeatureForUserRequest;
import com.moneylion.usermanagement.model.FeatureAccessibilityForUserResponse;
import com.moneylion.usermanagement.constants.AppConstants;
import com.moneylion.usermanagement.entities.UsersFeaturesEntity;
import com.moneylion.usermanagement.exceptions.UserManagementServiceException;
import com.moneylion.usermanagement.repositories.UsersFeaturesRepository;
import com.moneylion.usermanagement.resources.ManageUsersResource;

/**
 * Indicates that an annotated class is a "Service", originally defined by
 * Domain-Driven Design (Evans, 2003) as "an operation offered as an interface
 * that stands alone in the model, with no encapsulated state."
 *
 * <p>
 * May also indicate that a class is a "Business Service Facade" (in the Core
 * J2EE patterns sense), or something similar. This annotation is a
 * general-purpose stereotype and individual teams may narrow their semantics
 * and use as appropriate.
 *
 * This class contains the business logic to add,update or fetch the feature of
 * user from the database.
 * 
 */
@Service
public class ManageUsersServiceImpl implements ManageUsersService {

	/**
	 * Initialising the logger instance.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(ManageUsersResource.class);

	private final UsersFeaturesRepository usersFeaturesRepository;

	/**
	 * Dependency Inject using Autowired annotation which is alternative of inject
	 * annotation.
	 */
	@Autowired
	ModelMapper modelMapper;

	/**
	 * Constructor Injection for the dependency.
	 * 
	 * @see https://docs.spring.io/spring-boot/docs/2.0.x/reference/html/using-boot-spring-beans-and-dependency-injection.html
	 */
	ManageUsersServiceImpl(final UsersFeaturesRepository usersFeaturesRepository) {
		this.usersFeaturesRepository = usersFeaturesRepository;
	}

	/**
	 * This function take two parameters and queries the database if we have a
	 * record for the given email and featureName depending on the presence of data
	 * it creates the response.
	 * 
	 * @param username    - This is actually the email id of the user.
	 * @param featureName - This is the feature which is associated to the user.
	 * 
	 * @return Object of Type FeatureAccessibilityForUserResponse example -
	 *         {"canAccess":true}
	 */
	@Override
	public FeatureAccessibilityForUserResponse fetchFeatureAccessibilityForUser(String username, String featureName) {
		String logPrefix = "##fetchFeatureAccessibilityForUser(): ";
		LOGGER.debug("{} : Enter", logPrefix);
		Optional<UsersFeaturesEntity> usersFeaturesEntityOptional = usersFeaturesRepository
				.findByFeatureNameAndEmail(featureName, username);
		FeatureAccessibilityForUserResponse featureAccessibilityForUserResponse = new FeatureAccessibilityForUserResponse(
				false);
		if (usersFeaturesEntityOptional.isPresent()) {
			if(usersFeaturesEntityOptional.get().getEnable().equalsIgnoreCase("true")) {
				featureAccessibilityForUserResponse.setCanAccess(true);
			} else featureAccessibilityForUserResponse.setCanAccess(false);
		}
		LOGGER.debug("{} : Enter", logPrefix);
		return featureAccessibilityForUserResponse;
	}

	/**
	 * This function takes the object CreateFeatureForUserRequest as an input and
	 * checks if the data is present for that user in the data base. If data is
	 * already there it will update the record with new data. Otherwise it will
	 * create new record in the database.
	 * 
	 * @param createFeatureForUserRequest - Object of Type
	 *                                    CreateFeatureForUserRequest.
	 * 
	 * @return Object of Type FeatureAccessibilityForUserResponse example -
	 *         {"canAccess":true}
	 */
	@Override
	public ResponseEntity<String> saveNewFeatureForUser(CreateFeatureForUserRequest createFeatureForUserRequest) {
		String logPrefix = "##saveNewFeatureForUser(): ";
		LOGGER.debug("{} : Enter", logPrefix);
		try {
			validateFieldsInRequest(createFeatureForUserRequest);
		} catch (UserManagementServiceException umex) {
			return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
		}
		Optional<UsersFeaturesEntity> usersFeaturesEntityOptional = usersFeaturesRepository.findByFeatureNameAndEmail(
				createFeatureForUserRequest.getFeatureName(), createFeatureForUserRequest.getEmail());
		LOGGER.debug("{} : Exit", logPrefix);
		return usersFeaturesEntityOptional.isPresent()
				? updateAndSaveUserFeaturesDetails(usersFeaturesEntityOptional.get(), createFeatureForUserRequest)
				: saveUserFeaturesDetails(createFeatureForUserRequest);
	}

	/**
	 * 
	 * This is the helper method for saveNewFeatureForUser method. It updates the
	 * record into the database.
	 * 
	 * @param createFeatureForUserRequest - Object of Type
	 *                                    CreateFeatureForUserRequest.
	 * 
	 * @return ResponseEntity
	 */
	private ResponseEntity<String> updateAndSaveUserFeaturesDetails(UsersFeaturesEntity usersFeaturesEntity,
			CreateFeatureForUserRequest createFeatureForUserRequest) {
		String logPrefix = "##updateAndSaveUserFeaturesDetails(): ";
		LOGGER.debug("{} : Enter", logPrefix);
		usersFeaturesEntity.setEmail(createFeatureForUserRequest.getEmail());
		usersFeaturesEntity.setEnable(createFeatureForUserRequest.getEnable());
		usersFeaturesEntity.setFeatureName(createFeatureForUserRequest.getFeatureName());
		usersFeaturesRepository.saveAndFlush(usersFeaturesEntity);
		LOGGER.debug("{} : Users Features Updated Into Database With Values [{}]", logPrefix,
				usersFeaturesEntity.toString());
		LOGGER.debug("{} : Exit", logPrefix);
		return new ResponseEntity<String>(AppConstants.RESPONSE_MESSAGE_UPDATE, HttpStatus.CREATED);
	}

	/**
	 * 
	 * This is the helper method for saveNewFeatureForUser method. It create new
	 * record into the database.
	 * 
	 * @param createFeatureForUserRequest - Object of Type
	 *                                    CreateFeatureForUserRequest.
	 * 
	 * @return ResponseEntity
	 */
	private ResponseEntity<String> saveUserFeaturesDetails(CreateFeatureForUserRequest createFeatureForUserRequest) {
		String logPrefix = "##saveUserFeaturesDetails(): ";
		LOGGER.debug("{} : Enter", logPrefix);
		UsersFeaturesEntity usersFeaturesEntity = modelMapper.map(createFeatureForUserRequest,
				UsersFeaturesEntity.class);
		usersFeaturesRepository.save(usersFeaturesEntity);
		LOGGER.debug("{} : Users Features Added Into Database With Values [{}]", logPrefix,
				usersFeaturesEntity.toString());
		LOGGER.debug("{} : Exit", logPrefix);
		return new ResponseEntity<String>(AppConstants.RESPONSE_MESSAGE_CREATED, HttpStatus.CREATED);
	}

	/**
	 * A helper method to validate the incoming request.
	 * 
	 * @param createFeatureForUserRequest Type :- CreateFeatureForUserRequest
	 */
	private void validateFieldsInRequest(CreateFeatureForUserRequest createFeatureForUserRequest) {
		if (Optional.ofNullable(createFeatureForUserRequest).isPresent()) {
			if (!Optional.ofNullable(createFeatureForUserRequest.getEmail()).isPresent()) {
				throw new UserManagementServiceException(HttpStatus.BAD_REQUEST, "Email field cannot be null");
			} else if (!Optional.ofNullable(createFeatureForUserRequest.getEnable()).isPresent()) {
				throw new UserManagementServiceException(HttpStatus.BAD_REQUEST, "Enable field cannot be null");
			} else if (!Optional.ofNullable(createFeatureForUserRequest.getFeatureName()).isPresent()) {
				throw new UserManagementServiceException(HttpStatus.BAD_REQUEST, "Feature Name field cannot be null");
			}
		} else {
			throw new UserManagementServiceException(HttpStatus.BAD_REQUEST, "Request Object Cannot be null");
		}
	}
}
