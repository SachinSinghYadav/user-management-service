package com.moneylion.usermanagement.services;

import org.springframework.http.ResponseEntity;

import com.moneylion.usermanagement.model.CreateFeatureForUserRequest;
import com.moneylion.usermanagement.model.FeatureAccessibilityForUserResponse;

/**
 * Blue Print of Services available in ManageUsersSerivce.
 * For Implementation details please go to the implementing class.
 * 
 * @author sachinsinghyadav
 * */
public interface ManageUsersService {

	FeatureAccessibilityForUserResponse fetchFeatureAccessibilityForUser(String username , String featureName);
	
	ResponseEntity<String> saveNewFeatureForUser(CreateFeatureForUserRequest createFeatureForUserRequest);
}
