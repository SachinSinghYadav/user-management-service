package com.moneylion.usermanagement.configurations;


//+ 3rd Party Import(s)
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 
 * RdbmsConnectionDetails is annotated  with @ConfigurationProperties for externalized configuration. 
 * Add this to a class definition or a {@code @Bean} method in a {@code @Configuration} 
 * class if you want to bind and validate some external Properties 
 * (e.g. from a application.properties file or application.yml file).
 * 
 * @author sachinsinghyadav
 * 
 * */
@Getter
@Setter
@Configuration
@ConfigurationProperties("spring.datasource")
public class RdbmsConnectionDetails {
  private String driverClassName;
  private String url;
  private String username;
  @ToString.Exclude
  private String password;
}
