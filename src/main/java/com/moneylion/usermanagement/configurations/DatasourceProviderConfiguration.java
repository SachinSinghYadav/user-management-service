package com.moneylion.usermanagement.configurations;

//+ 3rd Party Import(s)
import javax.sql.DataSource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import lombok.extern.slf4j.Slf4j;

/**
 * DatasourceProviderConfiguration class creates the datasource for the application
 * @author sachinsinghyadav
 * */
@Configuration
@Slf4j
public class DatasourceProviderConfiguration {
	
	/**
	 * Method dataSource is annotated with the method level annotation @Bean which
	 * indicates that this method will create and object and we want spring to manage that 
	 * object.
	 * */
	@Bean
	public DataSource dataSource(final RdbmsConnectionDetails rdbmsConnectionDetails) { 
		log.info("Received Rdbms Connection details :: Username :{} , DbUrl :{} , DriverClass :{}",
			      rdbmsConnectionDetails.getUsername(), rdbmsConnectionDetails.getUrl(),
			      rdbmsConnectionDetails.getDriverClassName());
		final DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(rdbmsConnectionDetails.getDriverClassName());
        dataSource.setUrl(rdbmsConnectionDetails.getUrl());
        dataSource.setUsername(rdbmsConnectionDetails.getUsername());
        dataSource.setPassword(rdbmsConnectionDetails.getPassword());
		log.info("Datasource is instantiated successfully.");
	    return dataSource; 
	}	
}
