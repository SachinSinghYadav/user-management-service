package com.moneylion.usermanagement.repositories;

//+ Standard Import(s).
import java.util.Optional;

//+ 3rd Party Import(s)
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

//+ Local Import(s)
import com.moneylion.usermanagement.entities.UsersFeaturesEntity;

/**
 * JPA specific extension of {@link org.springframework.data.repository.Repository}.
 * 
 * @author sachinsinghyadav
 * */
@Repository
public interface UsersFeaturesRepository extends JpaRepository<UsersFeaturesEntity, Long> {
	
	Optional<UsersFeaturesEntity> findByFeatureNameAndEmail(String featureName , String email);
}
