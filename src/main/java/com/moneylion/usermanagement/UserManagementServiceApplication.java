package com.moneylion.usermanagement;

//+ 3rd Party Import(s)
import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


/**
 * Main Spring Boot Application Class. 
 * @author sachinsinghyadav
 * */

@SpringBootApplication(scanBasePackages = {"com.moneylion.usermanagement"})
@EnableJpaRepositories
public class UserManagementServiceApplication {

  public static void main(String... args) {
    SpringApplication.run(UserManagementServiceApplication.class, args);
  }
  
  /**
   * Method modelMapper is annotated with the method level annotation @Bean which
   * indicates that this method will create and object and we want spring to manage that 
   * object.
   **/
  @Bean
  public ModelMapper modelMapper() {
    return new ModelMapper();
  }
}
