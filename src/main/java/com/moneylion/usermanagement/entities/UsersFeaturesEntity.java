package com.moneylion.usermanagement.entities;

//+3rd Party Import(s)
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.Id;
import lombok.Getter;
import lombok.Setter;


/**
 * 
 * Class representation of users_feature table which holds the data for users along
 * with features and there accessibility.
 * 
 * @author sachinsinghyadav
 * 
 * */
@Getter
@Setter
@Entity
@Table(name = "UsersFeatures")
public class UsersFeaturesEntity {
	
	public static final String VARCHAR_10 = "VARCHAR(10)";
	public static final String VARCHAR_30 = "VARCHAR(30)";
	public static final String VARCHAR_5 = "VARCHAR(5)";
	
    @Id
    @Column(name = "Id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;
     
    @Column(columnDefinition = VARCHAR_10)
    public String featureName;
    
    @Column(columnDefinition = VARCHAR_30)
    public String email;
    
    @Column(columnDefinition = VARCHAR_5)
    public String enable;

}
