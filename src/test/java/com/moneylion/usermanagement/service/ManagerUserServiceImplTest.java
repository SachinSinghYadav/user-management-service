package com.moneylion.usermanagement.service;

import com.moneylion.usermanagement.model.CreateFeatureForUserRequest;
import com.moneylion.usermanagement.model.FeatureAccessibilityForUserResponse;
import com.moneylion.usermanagement.service.*;
import com.moneylion.usermanagement.services.ManageUsersService;

//+ Junit-5/Mockito Import(s)
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import static org.mockito.ArgumentMatchers.*;
import org.modelmapper.ModelMapper;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class ManagerUserServiceImplTest {

	@Spy
	ModelMapper modelMapper;
	
	@Mock
	ManageUsersService manageUsersService;
	
	@Before
    public void initMocks(){
        MockitoAnnotations.initMocks(this);
    }
	
	@Test
	public void fetchFeatureAccessibilityForUser_withValidInput_expectValidOutput() {
		FeatureAccessibilityForUserResponse featureAccessibilityForUserResponse = new FeatureAccessibilityForUserResponse(true);
		given(manageUsersService.fetchFeatureAccessibilityForUser("test@gmail.com", "testFeature")).willReturn(featureAccessibilityForUserResponse);
		Assertions.assertThat(manageUsersService.fetchFeatureAccessibilityForUser("test@gmail.com", "testFeature")).isEqualTo(featureAccessibilityForUserResponse);
	}
	
	@Test
	public void saveNewFeatureForUser_withValidInput_expectValidOutput() {
		ResponseEntity<String> responseEntity = new ResponseEntity("Created",HttpStatus.CREATED);
		CreateFeatureForUserRequest createFeatureForUserRequest = new CreateFeatureForUserRequest();
		createFeatureForUserRequest.setEmail("test@gmail.com");
		createFeatureForUserRequest.setEnable("true");
		createFeatureForUserRequest.setFeatureName("testFeature");
		given(manageUsersService.saveNewFeatureForUser(createFeatureForUserRequest)).willReturn(responseEntity);
		Assertions.assertThat(manageUsersService.saveNewFeatureForUser(createFeatureForUserRequest)).isEqualTo(responseEntity);
	}
	
}
