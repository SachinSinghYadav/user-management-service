package com.moneylion.usermanagement.resource;

//+ Junit Import(s)
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;
import org.junit.Assert;

//+ Local Import(s)
import com.moneylion.usermanagement.services.ManageUsersService;
import com.moneylion.usermanagement.resources.ManageUsersResource;
import com.moneylion.usermanagement.model.CreateFeatureForUserRequest;
import com.moneylion.usermanagement.model.FeatureAccessibilityForUserResponse;

@RunWith(MockitoJUnitRunner.class)
public class ManageUsersResourceTest {

	@Mock
	ManageUsersService manageUsersService;

	@Before
	public void setUp() {
	}

	@Test
	public void getFeatureAccessibilityForUser_whenInputDataIsValid_expectValidResponse() {
		when(manageUsersService.fetchFeatureAccessibilityForUser(any(), any()))
				.thenReturn(new FeatureAccessibilityForUserResponse(true));
		ManageUsersResource manageUsersResource = new ManageUsersResource(manageUsersService);
		Assert.assertEquals(manageUsersResource.getFeatureAccessibilityForUser("test@gmail.com", "testFeature"),
				manageUsersService.fetchFeatureAccessibilityForUser("test@gmail.com", "testFeature"));
	}

	@Test
	@Ignore
	public void saveNewFeatureForUser_whenInputDataIsValid_expectValidResponse() {
		when(manageUsersService.saveNewFeatureForUser(createFeatureForUserRequest()))
				.thenReturn(new ResponseEntity<String>("Updated", HttpStatus.CREATED));
		ManageUsersResource manageUsersResource = new ManageUsersResource(manageUsersService);
		manageUsersService.saveNewFeatureForUser(createFeatureForUserRequest());

	}

	private CreateFeatureForUserRequest createFeatureForUserRequest() {
		CreateFeatureForUserRequest createFeatureForUserRequest = new CreateFeatureForUserRequest();
		createFeatureForUserRequest.setEmail("test@gmail.com");
		createFeatureForUserRequest.setEnable("true");
		createFeatureForUserRequest.setFeatureName("testFeature");
		return createFeatureForUserRequest;
	}

}
