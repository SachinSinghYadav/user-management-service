package com.moneylion.usermanagement.datasource;

//+ Junit Import(s)
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.sql.SQLException;
import javax.sql.DataSource;

//+ 3rd Party Import(s)
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

//+ Local Import(s)
import com.moneylion.usermanagement.configurations.DatasourceProviderConfiguration;
import com.moneylion.usermanagement.configurations.RdbmsConnectionDetails;



@RunWith(SpringJUnit4ClassRunner.class)
@EnableConfigurationProperties(value = RdbmsConnectionDetails.class)
@SpringBootTest(classes = {DatasourceProviderConfiguration.class, DataSourceProperties.class})
public class DatasourceProviderConfigurationTest {
	
	@Autowired
	DataSource dataSource;
	
	@Before
	public void setUp() {		
	}
	
	@Test
	@Ignore
	public void dataSource_createDataSource_expectDatasource() throws SQLException {
	    assertThat(dataSource.getConnection().getMetaData().toString().contains("jdbc:h2:mem:um")).isTrue();
	    assertThat(dataSource.getConnection().getMetaData().toString().contains("jdbc:h2:mem:um1")).isFalse();
	}	
	
}
